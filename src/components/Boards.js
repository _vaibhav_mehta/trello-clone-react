import React, { Component } from "react";
import Board from "./Board";
import close from "../times-solid.svg";

import "../App.css";
const key = "7f18079a16f66385c196d485b78f7931";
const token =
  "4e0815794720b4af8bf653fb1838b7f8f90d0a14195813e587299ae1f1345029";
class Boards extends Component {
  state = {
    boards: [],
    visibile: false,
  };
  componentDidMount() {
    fetch(
      `https://api.trello.com/1/members/vaibhavmehta23/boards?key=${key}&token=${token}`,
      {
        method: "GET",
      }
    )
      .then((data) => data.json())
      .then((data) => {
        console.log(data);
        this.setState({
          boards: data,
        });
      });
  }
  addBoard(title) {
    return new Promise ((resolve,reject)=>{
        fetch(`https://api.trello.com/1/boards/?name=${title}&key=${key}&token=${token}`, {
        method: "POST",
        })
        .then((response) => {
            console.log(`Response: ${response.status} ${response.statusText}`);
            return response.json();
        })
        .then((text) => {
            console.log("new board id is"+text.id);
            resolve(text);
        })
        .catch((err) => console.error(err));
    })
  }

  newBoard() {
    var title=document.getElementsByClassName("board-title")[0].value;
    title.trim();
    if(title!=null)
    {
       // e.preventDefault();
        this.addBoard(title)
        .then((newBoard)=>{
            // var cards=document.createElement("div");
            // cards.className="newCard";
            console.log(newBoard);
            var {boards}=this.state
            var updatetedBoards=boards.concat(newBoard)
            this.setState({visible:false,boards:updatetedBoards});
        })
    }

}
deleteBoard(boardId){
    //console.log(boardId);
    fetch(`https://api.trello.com/1/boards/${boardId}?key=${key}&token=${token}`, {
            method: 'DELETE'
        })
        .then(response => {
        console.log(
                `Response: ${response.status} ${response.statusText}`
            );
            return response.text();
            })
            .then(text => {
                var {boards}=this.state
                var updatetedBoards=boards.filter(board=>board.id !== boardId)
                this.setState({boards:updatetedBoards});

            })
            .catch(err => console.error(err));

}
  render() {
    var allBoards = this.state.boards.map((board) => {
      return (
        <div className="board-buttons ml-2 pt-2">
        <Board 
            key={board.id} 
            boards={board}
            deleteBoard={()=>this.deleteBoard(board.id)}
        />
       
        </div>
      );
    });
    return (
      <div>
        <h3> Personal Boards</h3>
        {allBoards}
        <div className="new-board">
          <div
            className={this.state.visible ? "invisible" : "add-board-button"}
          >
            <button
              type="button"
              class="btn btn-warning new-board"
              onClick={() => {
                  this.setState({ visible: !this.state.visible })
                  document.getElementsByClassName("board-title")[0].value="";
                }
                }
            >
              + Create new board
            </button>
          </div>
          <div
            className={
              this.state.visible ? "input-area-buton ml-1" : "invisible"
            }
          >
            <textarea
              className="board-title textArea mt-3"
              placeholder="Enter Board Title..."
            ></textarea>
            <div className="btn-addList">
              <button
                type="button"
                className="btn btn-success"
                onClick={() => this.newBoard()}
              >
                Add Board
              </button>
              <img
                className="close-image pl-2"
                src={close}
                alt="close button"
                height="20px"
                onClick={() => this.setState({ visible: false })}
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default Boards;
