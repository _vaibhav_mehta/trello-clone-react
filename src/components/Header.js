import React from 'react';
import {Link} from 'react-router-dom';
function Header(){
    return(
        <header className="header">
            <div className="left row">
                <Link to='/'>
                    <img className="home ml-4 mt-2" alt="homeIcon" src="https://img.icons8.com/android/24/000000/home.png"/>
                </Link>
                <Link to='/boards'>
                <button type="button" className="btn btn-primary mt-1 ml-2">Boards</button>
                </Link>
            </div>
            <div className="center">
                <img alt="trello" src="https://img.icons8.com/color/48/000000/trello.png"/>   
            </div>
        </header>
    )
}
export default Header;