import React from 'react';
import '../App.css';
import del from "../times-solid.svg";
function makeEditable(e){
    e.target.contentEditable=true;
}
const Card = props => {
    
    return (
        <div className="card mt-3">
            <h6 className="cardName" onClick={(e)=>makeEditable(e)} onKeyDown={(e)=>props.editCard(e)} >{props.cards.name}</h6>
            <img className="delCard"src={del} alt="delete card" onClick={props.deleteCard} height="20px"/>
        </div>    
    );
};
export default Card;