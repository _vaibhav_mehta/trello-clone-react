import React, { Component } from 'react';
import '../App.css';
import List from './List';
import close from '../times-solid.svg';
const key="7f18079a16f66385c196d485b78f7931";
const token="4e0815794720b4af8bf653fb1838b7f8f90d0a14195813e587299ae1f1345029";

class Lists extends Component{
   
        state={
        lists:[],
        visible:false,
        title:""
    };

    componentDidMount(){
        fetch(
            `https://api.trello.com/1/boards/${this.props.match.params.id}?key=${key}&token=${token}`
        )
        .then(data=>data.json())
        .then(data=>{
            this.setState({title:data.name},()=>{
                    fetch(
                    `https://api.trello.com/1/boards/${this.props.match.params.id}/lists?key=${key}&token=${token}`,{
                        method:'GET'
                    }
                    )
                    .then(data=>data.json())
                    .then(data=>{
                    console.log(data);
                    this.setState({
                        lists:data
                    })
                })
            })
        })
        
    }
    newList(boardId) {
        var title=document.getElementsByClassName("list-title")[0].value;
        title.trim();
        if(title!=null)
        {
           // console.log(title);
            this.addList(title,boardId)
            .then((newList)=>{
                   var {lists}=this.state
                   var updatetedlists=lists.concat(newList)
                   this.setState({visible:false,lists:updatetedlists});
             })
        }
    
    }
    
    addList (title,boardId) {
       //var listId=event.target.parentElement.parentElement.parentElement.getAttribute('id');
       return new Promise ((resolve,reject)=>{
        fetch(`https://api.trello.com/1/lists?name=${title}&idBoard=${boardId}&key=${key}&token=${token}&pos=bottom`, {
            method: 'POST'
        })
            .then(response => {
            console.log(
                `Response: ${response.status} ${response.statusText}`
            );
            return response.json();
            })
            .then(text => {
                console.log("id for new list is"+text.id);
                resolve (text);
            })
            .catch(err => console.error(err));
        })
    }
    deleteList(listId){
        fetch(`https://api.trello.com/1/lists/${listId}/closed?key=${key}&token=${token}&value=true`, {
            method: 'PUT'
        })
        .then(response => {
        console.log(
                `Response: ${response.status} ${response.statusText}`
            );
            return response.text();
            })
            .then(text => {
                var {lists}=this.state
                var updatetedLists=lists.filter(list=>list.id !== listId)
                this.setState({lists:updatetedLists});

            })
            .catch(err => console.error(err));
    }
    makeEditable(e){
        e.target.contentEditable=true;
   }
   updateBoard(e){
    if(e.keyCode === 13){
            e.preventDefault();
            e.persist();
            console.log(e.target.innerText);
            let newTitle=e.target.innerText; 
            fetch(`https://api.trello.com/1/boards/${this.props.match.params.id}?name=${newTitle}&key=${key}&token=${token}`, {
            method: 'PUT',
            headers: {
                'Accept': 'application/json'
            }
            })
            .then(response => {
                console.log(
                `Response: ${response.status} ${response.statusText}`
                );
                return response.json();
            })
            .then(text => {
                console.log(text);
                e.target.contentEditable=false;
                })
            .catch(err => console.error(err));
        }
    }
    render(){
        var allLists=this.state.lists.map(list =>{
            return(
                <div className="list-outer mr-3 ml-4" width="500px">
                    <List
                    key={list.id}
                    lists={list}
                    deleteList={()=>this.deleteList(list.id)}
                    />
                </div>
            );
        })
        return(
            <div className="board-container row ">
                <div className="title">
                    <h1 className="title-heading"onClick={(e)=>this.makeEditable(e)} onKeyDown={(e)=>this.updateBoard(e)}>
                        {this.state.title}
                    </h1>
                </div>
                
                {allLists}
                <div className={this.state.visible?"invisible":"addList mt-4"} width="230px" onClick={()=> {
                    this.setState({visible:!this.state.visible});
                    document.getElementsByClassName("list-title")[0].value="";
                    }}> 
                <strong>+ Add Another List </strong></div>
                <div className={this.state.visible?"addListPopUp":"invisible"}>
                    <div className="input-area-buton">
                        <textarea className="list-title textArea mt-3" placeholder="Enter list Title..."></textarea> 
                        <button type="button" className="btn btn-success" onClick={()=>this.newList(this.props.match.params.id)}>Add List</button>
                        <img className="close-image pl-2"src={close} alt="close button" height="20px" onClick={()=>this.setState({visible:false})}/>
                    </div>
                </div>
            </div>
            
        );
            
    }
}
export default Lists;
