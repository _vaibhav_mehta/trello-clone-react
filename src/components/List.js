
import React,{Component} from 'react';
import "../App.css";
import Card from './Card';
import close from '../times-solid.svg';
const key="7f18079a16f66385c196d485b78f7931";
const token="4e0815794720b4af8bf653fb1838b7f8f90d0a14195813e587299ae1f1345029";

class List extends Component{
    constructor(props){
        super(props);
        this.state={
            cards:[],
            isvisible:false,
            list:this.props.lists
        };
        }
    componentDidMount(){
        fetch(
            `https://api.trello.com/1/lists/${this.props.lists.id}/cards?key=${key}&token=${token}`,
            {
                method:"GET"
            }
        )
        .then(data=>data.json())
        .then(data=>{
            console.log(data);
            this.setState({
                cards:data
            });
        });

    }
    
    newCard(idList,e) {
        var title=document.getElementById(idList).value;
        title.trim();
        if(title!=null)
        {
           // e.preventDefault();
            this.addcard(title,idList)
            .then((newCard)=>{
                // var cards=document.createElement("div");
                // cards.className="newCard";
                console.log(newCard);
                var {cards}=this.state
                var updatetedCards=cards.concat(newCard)
                document.getElementById(idList).value="";
                this.setState({isvisible:false,cards:updatetedCards});
            })
        }
    
    }
    
    addcard (name,listId) {
       //var listId=event.target.parentElement.parentElement.parentElement.getAttribute('id');
       return new Promise ((resolve,reject)=>{
        fetch(`https://api.trello.com/1/cards?idList=${listId}&name=${name}&key=${key}&token=${token}`, {
            method: 'POST'
        })
            .then(response => {
            console.log(
                `Response: ${response.status} ${response.statusText}`
            );
            return response.json();
            })
            .then(text => {
                console.log("id for new card is"+text.id);
                resolve (text);
            })
            .catch(err => console.error(err));
        })
    }
    deleteCard(cardId){
            fetch(`https://api.trello.com/1/cards/${cardId}?key=${key}&token=${token}`, {
                method: 'DELETE'
            })
            .then(response => {
            console.log(
                    `Response: ${response.status} ${response.statusText}`
                );
                return response.text();
                })
                .then(text => {
                   
                    var {cards}=this.state
                    var updatetedCards=cards.filter(card=>card.id !== cardId)
                    this.setState({cards:updatetedCards});

                })
                .catch(err => console.error(err));
    }
    updateList(e){
         if(e.keyCode === 13){
             e.preventDefault();
             e.persist();
             console.log(e.target.innerText);
             let newTitle=e.target.innerText;          
            fetch(`https://api.trello.com/1/lists/${this.state.list.id}?name=${newTitle}&key=${key}&token=${token}`, {
             method: 'PUT'
            })
            .then(response => {
                console.log(
                `Response: ${response.status} ${response.statusText}`
                );
                return response.json();
            })
            .then(text => {
        
                let {list}=this.state;
                let updatedList=list;
                updatedList.name=text.name;
                this.setState({list:updatedList});
                e.target.contentEditable=false;
                })
            .catch(err => console.error(err));
        }
   }
   makeEditable(e){
        e.target.contentEditable=true;
   }
   editCard(e,cardid){
    if(e.keyCode === 13){
            e.preventDefault();
            e.persist();
            console.log(e.target.innerText);
            let newCard=e.target.innerText; 
            fetch(`https://api.trello.com/1/cards/${cardid}?name=${newCard}&key=${key}&token=${token}`, {
            method: 'PUT',
            headers: {
                'Accept': 'application/json'
            }
            })
            .then(response => {
                console.log(
                `Response: ${response.status} ${response.statusText}`
                );
                return response.json();
            })
            .then(text => {
                console.log(text);
                e.target.contentEditable=false;
                })
            .catch(err => console.error(err));
        }
    }
    
    render(){
        console.log(this.props);
        
        var allcards=this.state.cards.map(card=>{
            return (
                <Card
                    key={card.id}
                    cards={card}
                    deleteCard={()=>this.deleteCard(card.id)}
                    editCard={(e)=>this.editCard(e,card.id)}
                />
            );
        });
    
    return(
        <div>
            <div className="listDel">
                <h5 className="listName" onClick={(e)=>this.makeEditable(e)} onKeyDown={(e)=>this.updateList(e)} > {this.props.lists.name}</h5>
                <img  className="delListIcon" src="https://img.icons8.com/carbon-copy/100/000000/delete-forever--v1.png" alt= "delete list" height="25px" onClick={this.props.deleteList}/>
            </div>
            <div className="cards-outer">
                    {allcards}
                    <div className={this.state.isvisible ? "invisible":"add-button mt-2"} onClick={()=> {
                        
                        this.setState({isvisible:!this.state.isvisible});
                        }}> + Add another card</div> 

                    <div className={this.state.isvisible?"input-area-buton":"invisible"}>
                        <textarea placeholder="Enter a title for this card..."id={this.props.lists.id} className="card-title textArea mt-3"></textarea> 
                        <button type="button" className="btn btn-success" onClick={()=>this.newCard(this.props.lists.id)}>Add card</button>
                        <img className="close-image pl-2"src={close} alt="close button" height="20px" onClick={()=>this.setState({isvisible:false})}/>
                    </div>
            </div>  
        </div>
    
    )}

}
export default List