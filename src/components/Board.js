import React from 'react';
import {Link} from 'react-router-dom';
import "../App.css";    
const Board= props =>{
    return(
        <div className="board">
            <Link to={`/board/${props.boards.id}`}>
                <div type="button" className='boardbutton btn btn-primary active'>
                    {props.boards.name}   
                </div>
            </Link>
            <img className="delete-board ml-1"src="https://img.icons8.com/carbon-copy/100/000000/delete-forever--v1.png" onClick={props.deleteBoard} alt="delete board" height="25px"  />
        </div>
    );
};
 
export default Board;