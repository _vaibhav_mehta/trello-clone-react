import React from 'react';
//import logo from './logo.svg';
import './App.css';
import Header from './components/Header';
import Home from './components/Home';
import Boards from './components/Boards';
import Lists from './components/Lists';

import {
  BrowserRouter as Router,
  Route, Switch
} from "react-router-dom";


function App() {
  return (
    <div className="App">
      <Router>
            <React.Fragment>
              <Header />
              <Switch>
                  <Route path ="/" exact component={Home}/>
                  <Route path ="/boards" exact component={Boards}/>
                  <Route path ="/board/:id" exact component={Lists}/> 
              </Switch>           
            </React.Fragment> 
      </Router>
    </div>
  );
}

export default App;
